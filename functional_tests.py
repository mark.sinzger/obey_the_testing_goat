from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest

class NewVisitorTest(unittest.TestCase): #inherit from unittest.TestCase
    def setUp(self):
        # open the Firefox browser
        self.browser = webdriver.Firefox()
    def tearDown(self):
        self.browser.quit()
    def test_to_do_basic(self):
        # with the following website address
        self.browser.get('http://localhost:8000')

        # test: to-do is in the title? And header?
        self.assertIn ('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)
        #dialogue request to input to-do item
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(inputbox.get_attribute('placeholder'),
            'Enter To-Do item'
        )
        # enter a to-do item
        inputbox.send_keys('Send Lumi a Postcard')
        # hit enter -> one to-do item in list
        inputbox.send_keys(Keys.ENTER)
        time.sleep(1)
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
            any(row.text == '1. Send Lumi a Postcard' for row in rows)
        )
        # text box invites to add another item. enter item2

        # page update -> two items in list

        # generate a unique URL, show some explanatory text for user

        # visit that URL -> to-do list is there.
        self.fail('Go home and sheep')

if __name__ == '__main__':
    unittest.main()





